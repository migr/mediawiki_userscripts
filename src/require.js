// This file is maintained at https://gitlab.wikimedia.org/migr/mediawiki_userscripts
/**
 * This adds two methods to the global window object: preloadDependency() and require()
 *
 * A common.js script/file/module stored on the current wiki (location.host) can be asynchronously loaded with
 * `preloadDependency(<title of the page containing the module>)` and then anywhere else synchronously accessed with
 * `require(<title of the page containing the module>)`
 *
 * `require(<resource loader module>)` still works as expected as well, that is, resource-loader must have loaded the
 * module already (`mw.loader.getState(<resource loader module>) === 'ready')
 */
(function () {
  /* global mw */
  mw.loader.using('mediawiki.api', (require) => {
    window.mwLoaderRequire = require;
  });

  function validateModuleName(moduleName) {
    if (!(moduleName.endsWith('.js') || moduleName.endsWith('.vue'))) {
      throw new Error(`"${moduleName}", missing ".js" or ".vue" suffix!`);
    }
  }

  function getPreloadedModuleText(moduleName) {
    const nameParts = moduleName.split('/');
    let module = window.preloadedDependencies;
    try {
      for (const part of nameParts) {
        module = module[part];
      }
    } catch (e) {
      console.error(
        `Failed loading ${moduleName}! Has it been preloaded with preloadDependency()?`,
        e,
      );
      throw new Error(`Failed loading module ${moduleName}`);
    }

    if (!module) {
      console.error(
        `Failed loading ${moduleName}! Has it been preloaded with preloadDependency()?`,
      );
      throw new Error(`Failed loading module ${moduleName}`);
    }

    return module;
  }

  function storeModuleText(moduleName, moduleText) {
    const nameParts = moduleName.split('/');
    const scriptName = nameParts.pop();
    if (!window.preloadedDependencies) {
      window.preloadedDependencies = {};
    }
    let ref = window.preloadedDependencies;
    for (const part of nameParts) {
      if (typeof ref[part] === 'undefined') {
        ref[part] = {};
      }
      ref = ref[part];
    }
    ref[scriptName] = moduleText;
  }

  function executeModuleText(moduleText) {
    const module = {};
    Function('module', moduleText)(module);
    return module.exports;
  }

  function executeVueModuleText(moduleText) {
    const templateMatch = moduleText.match(/<template>([\s\S]+)<\/template>/);
    const scriptMatch = moduleText.match(/<script>([\s\S]+)<\/script>/);
    const styleMatch = moduleText.match(/<style>([\s\S]+)<\/style>/);

    if (!templateMatch || !scriptMatch) {
      throw new Error('Vue module must have a template and a script tag!');
    }

    const template = templateMatch[1];
    let script = scriptMatch[1];
    script = script.replaceAll(
      /import (.*) from ([.'@:A-Za-z\\/]+);/g,
      'const $1 = require($2);',
    );
    script = script.replace('export default ', 'module.exports = ');
    const style = styleMatch ? styleMatch[0] : '';

    const module = {};
    try {
      Function('module', script)(module);
    } catch (e) {
      console.error('Failed executing script', script);
      throw e;
    }
    module.exports.template = style + template;
    return module.exports;
  }

  window.require = function require(moduleName) {
    const moduleState = mw.loader.getState(moduleName);
    if (moduleState === 'ready') {
      return window.mwLoaderRequire(moduleName);
    }
    if (moduleState !== null) {
      throw new Error(`state for ${moduleName} is ${moduleState}`);
    }

    validateModuleName(moduleName);

    const moduleText = getPreloadedModuleText(moduleName);

    if (moduleName.endsWith('.vue')) {
      return executeVueModuleText(moduleText);
    }

    return executeModuleText(moduleText);
  };

  window.preloadDependency = async function preloadDependency(moduleName) {
    validateModuleName(moduleName);
    const host = location.host;
    const url = `//${host}/w/index.php?title=${moduleName}&action=raw`;
    // TODO: make sure caching works (must revalidate)!
    return fetch(url).then(
      async function (response) {
        const scriptText = await response.text();
        if (!scriptText) {
          throw new Error(
            `Failed preloading module ${moduleName}! Module is empty!`,
          );
        }
        storeModuleText(moduleName, scriptText);
      },
      (...errorParams) => {
        console.error(...errorParams);
      },
    );
  };
})();
