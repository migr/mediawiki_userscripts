class NewEntityRepository {
  #api;
  #langCode;

  constructor(api, langCode) {
    this.#api = api;
    this.#langCode = langCode;
  }

  saveNewItem(terms, statements) {
    const params = {
      action: 'wbeditentity',
      new: 'item',
      data: JSON.stringify({
        ...terms,
        claims: statements,
      }),
    };

    return this.#storeEntity(params);
  }

  async #storeEntity(params) {
    const response = await this.#api.postWithEditToken(
      this.#api.assertCurrentUser(params),
    );

    return response.entity.id;
  }
}

module.exports = NewEntityRepository;
