### Sonar:

These results from sonar cloud are based on the [GitHub mirror](https://github.com/micgro42/mediawiki_userscripts):

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=micgro42_mediawiki_userscripts&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=micgro42_mediawiki_userscripts)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=micgro42_mediawiki_userscripts&metric=bugs)](https://sonarcloud.io/summary/new_code?id=micgro42_mediawiki_userscripts)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=micgro42_mediawiki_userscripts&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=micgro42_mediawiki_userscripts)

See also: https://sonarcloud.io/summary/overall?id=micgro42_mediawiki_userscripts

### Directory Structure:

- `src/components/`: generic non app-specific Wikidata components
- `src/repositories/`: generic non app-specific Wikidata repositories
- `src/app/<some app>/`: app-specific components
